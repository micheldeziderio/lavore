<!DOCTYPE html>
<html lang="pt-br">
<head>
<style>

	.alerta-sucesso{
		background:rgb(24,192,27);
		padding:10px 20px;
		color:#fff;
		max-width: 450px;
		border-radius: 5px;
		margin-top: 30px;
		margin-left: 20px;
	}

	.alerta-sucesso a{
		position: absolute;
		color:#000;
		padding: 10px;
		margin-top: 20px!important; 
		margin-left: -20px;
		background:silver;
		border-radius: 5px;
		text-decoration: none;
	}

	.alerta-sucesso a:hover{
		color:#fff;
	}


	.alerta-erro{
		background:rgb(238,93,93);
		padding:10px 20px;
		color:#fff;
		max-width: 450px;
		border-radius: 5px;
		margin-top: 30px;
		margin-left: 20px;
	}

	.alerta-erro a{
		position: absolute;
		color:#000;
		padding: 10px;
		margin-top: 20px!important; 
		margin-left: -20px;
		background:silver;
		border-radius: 5px;
		text-decoration: none;
	}

	.alerta-erro a:hover{
		color:#fff;
	}

</style>
</head>

<body>

<?php
ini_set('display_errors', 0 );
error_reporting(0);

if (isset($_POST['BTEnvia'])) {
	
	//Variaveis de POST, Alterar somente se necessário 
	//====================================================
	$nome = $_POST['nome'];
	$email = $_POST['email'];
	$assunto = $_POST['assunto']; 
	$mensagem = $_POST['mensagem'];
	//====================================================
	
	//REMETENTE --> ESTE EMAIL TEM QUE SER VALIDO DO DOMINIO
	//==================================================== 
	$email_remetente = "$email"; // deve ser uma conta de email do seu dominio 
	//====================================================
	
	//Configurações do email, ajustar conforme necessidade
	//==================================================== 
	$email_destinatario = "contato@lavoreservicos.com.br"; // pode ser qualquer email que receberá as mensagens
	$email_reply = "$email"; 
	$email_assunto = $assunto; // Este será o assunto da mensagem
	//====================================================
	
	//Monta o Corpo da Mensagem
	//====================================================
	$email_conteudo = "Nome: $nome \n"; 
	$email_conteudo .= "Email: $email \n";
	$email_conteudo .= "<br>Mensagem: $mensagem \n"; 
	//====================================================
	
	//Seta os Headers (Alterar somente caso necessario) 
	//==================================================== 
	$email_headers = implode ( "\n",array ( "From: $email_remetente", "Reply-To: $email_reply", "Return-Path: $email_remetente","MIME-Version: 1.0","X-Priority: 3","Content-Type: text/html; charset=UTF-8" ) );
	//====================================================
	
	//Enviando o email 
	//==================================================== 
	if (mail ($email_destinatario, $email_assunto, nl2br($email_conteudo), $email_headers)){ 
					//header('Location: http://www.lavoreservicos.com.br/portfolio/'); ; 

					echo "<div class='alerta-sucesso'> Sua mensagem foi enviada com sucesso. Obrigado! :) <br> <a href='http://www.lavoreservicos.com.br/'>Voltar</a> </div>";;

					} 
			else{ 
				echo "<div class='alerta-erro'> Sua mensagem não pode ser enviada. Tente novamente mais tarde! <br> <a href='http://www.lavoreservicos.com.br/'>Voltar</a> </div>";} 
	//====================================================

	//header('Localização: http://www.lavoreservicos.com.br/portfolio/');  


} 

?>


</body>
</html>
