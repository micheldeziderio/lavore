<!DOCTYPE html>
<html lang="pt-br">
<head>
  <meta charset="utf-8">
  <title>Lavore - Serviços Especializado | lavorecervicos.com.br</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="Marmores, Marmoraria, Granitos, Pedras, Procelanatos, Serviços Especializados, Lavore." name="keywords">
  <meta content="A LAVORE SERVIÇOS ESPECIALIZADOS, atua no mercado de Mármores e Granitos há 6 anos com uma equipe especializada, qualificada e de confiança para poder melhor atendê-lo no segmento de seu projeto, com o design ideal e sem risco de defeitos." name="description">

  <!-- Favicons -->
  <link href="img/favicon.png" rel="icon">
  <link href="img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Open+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/animate/animate.min.css" rel="stylesheet">
  <link href="lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- =======================================================
    Theme Name: LAVORE SERVIÇOS ESPECIALIZADOS
    Author: www.micheldeziderio.com.br
  ======================================================= -->
</head>

<body>

  <!--==========================
    Header
  ============================-->
  <header id="header">
    <div class="container">

      <div id="logo" class="pull-left">
        <!--<h1><a href="#intro" class="scrollto">Lavore</a></h1>-->
        <!-- Uncomment below if you prefer to use an image logo -->
        <h1><a href="#intro"><img src="img/icone-header.png" alt="" title="" width="180px"></a></h1>
      </div>

      <nav id="nav-menu-container">
        <ul class="nav-menu">
          <li class="menu-active"><a href="#intro">Home</a></li>
          <li><a href="#about">Quem somos</a></li>
          <li><a href="#features">Serviços</a></li>
          <li><a href="#gallery">Fotos</a></li>
          <li><a href="#contact">Contato</a></li>
        </ul>
      </nav><!-- #nav-menu-container -->
    </div>
  </header><!-- #header -->

  <!--==========================
    Intro Section
  ============================-->
  <section id="intro">

    <div class="intro-text">
      <h2>Seja bem vindo!</h2>
      <p>Aqui você encontra trabalho e material de qualidade.</p>
      <a href="#about" class="btn-get-started scrollto">Clique aqui para ver</a>
    </div>

    <div class="product-screens">

      <div class="product-screen-1 wow fadeInUp" data-wow-delay="0.4s" data-wow-duration="0.6s">
        <img src="img/product-screen-1.png" alt="">
      </div>

      <div class="product-screen-2 wow fadeInUp" data-wow-delay="0.2s" data-wow-duration="0.6s">
        <img src="img/product-screen-2.png" alt="">
      </div>

      <div class="product-screen-3 wow fadeInUp" data-wow-duration="0.6s">
        <img src="img/product-screen-3.png" alt="">
      </div>

    </div>

  </section><!-- #intro -->

  <main id="main">

    <!--==========================
      About Us Section
    ============================-->
    <section id="about" class="section-bg">
      <div class="container-fluid">
        <div class="section-header">
          <h3 class="section-title">Quem somos</h3>
          <span class="section-divider"></span>

        </div>

        <div class="row">
          <div class="col-lg-6 about-img wow fadeInLeft">
            <img src="img/about-img.jpg" alt="">
          </div>

          <div class="col-lg-6 content wow fadeInRight">
            <h2>Descubra um pouco sobre a <span style="color:#2049ae;">LAVORE</span>:</h2>
            <h3>Para sempre manter um vínculo com nossos clientes, nós utilizamos sempre os melhores materiais para maior satisfação.</h3>
            <p>
              A <span style="color:#2049ae;">LAVORE SERVIÇOS ESPECIALIZADOS</span>, atua no mercado de Mármores e Granitos há 6 anos com uma equipe especializada, qualificada e de confiança para poder melhor atendê-lo no segmento de seu projeto, com o design ideal e sem risco de defeitos.
            </p>

            <p>
              Também auxiliamos no seus projetos futuros de arquitetura e design, tendo como material, Marmores, Granitos e Porcelanato de alta qualidade. Fabricando (Fachadas, Escadas, Pias, Pisos, Lavatórios  e etc).
            </p>

            <p>
              Criamos móveis em pedras de acordo com as especificações do cliente. Fabricamos fachadas, escadas, pias, pisos e lavatórios. Sem contar que, também auxiliamos em seus projetos futuros de arquitetura e design, tendo como principais materiais os mármores, granitos e porcelanatos de alta qualidade.
            </p>

            <p>
              Venha nos fazer uma visita sem compromisso ou envie uma mensagem. Adoraremos te receber!
              <a class="cta-btn align-middle scrollto" href="#contact" style="font-weight: 500; font-size: 16px;  letter-spacing: 1px; background-color:#2049ae;  display: inline-block;  padding: 8px 30px; border-radius: 25px; transition: background 0.5s;  margin: 10px;  border: 2px solid #fff; color: #fff;">Enviar Mensagem</a>
            </p>
            
            <p>
              Estamos localizados na região do Horto Florestal.
            </p>

          </div>
        </div>

      </div>
    </section><!-- #about -->

    <!--==========================
      Product Featuress Section
    ============================-->
    <section id="features">
      <div class="container">

        <div class="row">

          <div class="col-lg-8 offset-lg-4">
            <div class="section-header wow fadeIn" data-wow-duration="1s">
              <h3 class="section-title">Nosso case de sucesso</h3>
              <span class="section-divider"></span>
            </div>
          </div>

          <div class="col-lg-4 col-md-5 features-img">
            <img src="img/product-features.png" alt="" class="wow fadeInLeft">
          </div>

          <div class="col-lg-8 col-md-7 ">

            <div class="row">

              <div class="col-lg-6 col-md-6 box wow fadeInRight">
                <div class="icon"><i class="ion-ios-speedometer-outline"></i></div>
                <h4 class="title">Metodo Ágil</h4>
                <p class="description">Realizamos e entregamos todos os nossos trabalhos no prazo e com o melhor desempenho.</p>
              </div>
              <div class="col-lg-6 col-md-6 box wow fadeInRight" data-wow-delay="0.1s">
                <div class="icon"><i class="ion-ios-home-outline"></i></div>
                <h4 class="title">Sua Segurança</h4>
                <p class="description">Nossa equipe é composta por pessoas comprometidas em realizar um trabalho bem feito e em manter a segurança de seu lar.</p>
              </div>
              <div class="col-lg-6 col-md-6 box wow fadeInRight box-center" data-wow-delay="0.2s">
                <div class="icon"><i class="ion-checkmark-round"></i></div>
                <h4 class="title">Qualidade</h4>
                <p class="description">Utilizamos o melhor material para levar qualidade aos nossos clientes.</p>
              </div>
              
            </div>

          </div>

        </div>

      </div>

    </section><!-- #features -->

    <!--==========================
      Product Advanced Featuress Section
    ============================-->
    <section id="advanced-features">

      <div class="features-row section-bg">
        <div class="container">
            <div class="section-header">
                <h3 class="section-title">Serviços que prestamos</h3>
                <span class="section-divider"></span>
      
              </div>
          <div class="row">
              <div class="col-12">
              <img class="advanced-feature-img-right wow fadeInRight" src="img/advanced-feature-1.jpg" alt="">
              <div class="wow fadeInLeft">
                <h2 clss="text-decoration-service"><div class="icon"><i class="ion-checkmark-round number-one"></i></div> Mão de obra e instalação</h2>
                <h2 clss="text-decoration-service"><div class="icon"><i class="ion-checkmark-round"></i></div> Tratamento (veja se fica melhor colocado) de mármore, granito, quartzo, porcelanato, cerâmica, entre outros.</h2>
                <h2 clss="text-decoration-service"><div class="icon"><i class="ion-checkmark-round"></i></div> Instalação (mema coisa, ve se se encaixa) de pisos em mármore, granitos, quartzo, porcelanato, cerâmica, entre outros.</h2>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="features-row">
        <div class="container">
          <div class="row">
            <div class="col-12">
              <img class="advanced-feature-img-left" src="img/advanced-feature-2.jpg" alt="">
              <div class="wow fadeInRight">
                <h2 clss="text-decoration-service"><div class="icon"><i class="ion-checkmark-round"></i></div> Revestimento em paredes com mármore, granitos, porcelanato, entre outros.</h2>
                <h2 clss="text-decoration-service"><div class="icon"><i class="ion-checkmark-round"></i></div> Instalação de pedras naturais, como a caminha, Miracema, pedras Goiás,pedra madeira, entre outras.</h2>
                <h2 clss="text-decoration-service"><div class="icon"><i class="ion-checkmark-round number-one"></i></div> Pinturas e reparo em geral.</h2>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section><!-- #advanced-features -->

    <!--==========================
      Call To Action Section
    ============================-->
    <section id="call-to-action">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 text-center text-lg-left">
            <h3 class="cta-title">Por que nos contratar?</h3>
            <p class="cta-text"> Temos uma equipe especializada e produtos de qualidade, oferecendo o melhor, sendo assim, entregamos projetos precisos e de forma ágil.</p>
          </div>
          <div class="col-lg-3 cta-btn-container text-center">
            <a class="cta-btn align-middle scrollto" href="#contact">Entrar em contato</a>
          </div>
        </div>

      </div>
    </section><!-- #call-to-action -->

    
    <!--==========================
      Clients
    ============================-->
    <!--
    <section id="clients">
      <div class="container">

        <div class="row wow fadeInUp">

          <div class="col-md-2">
            <img src="img/clients/client-1.png" alt="">
          </div>

          <div class="col-md-2">
            <img src="img/clients/client-2.png" alt="">
          </div>

          <div class="col-md-2">
            <img src="img/clients/client-3.png" alt="">
          </div>

          <div class="col-md-2">
            <img src="img/clients/client-4.png" alt="">
          </div>

          <div class="col-md-2">
            <img src="img/clients/client-5.png" alt="">
          </div>

          <div class="col-md-2">
            <img src="img/clients/client-6.png" alt="">
          </div>

        </div>
      </div>
    </section>
    -->


    <!--==========================
      Gallery Section
    ============================-->
    
    <section id="gallery">
      <div class="container-fluid">
        <div class="section-header">
          <h3 class="section-title">Fotos</h3>
          <span class="section-divider"></span>
          <p class="section-description">Conheça um pouco dos nossos trabalhos realizados.</p>
        </div>

        <div class="row no-gutters">
            <?php include 'instagram-fotos.php' ?>
        </div>

      </div>
    </section>


    <!--==========================
      Contact Section
    ============================-->
    <section id="contact">
      <div class="container">
        <div class="row wow fadeInUp">

          <div class="col-lg-4 col-md-4">
            <div class="contact-about">
              <h3><strong>lavore</strong></h3>
              <p>A <span style="color:#2049ae;">LAVORE SERVIÇOS ESPECIALIZADOS</span>, atua no mercado de Mármores e Granitos há 6 anos com uma equipe especializada, qualificada e de confiança para poder melhor atendê-lo no segmento de seu projeto, com o design ideal e sem risco de defeitos.</p>
              <div class="social-links">
<!--                
                <a href="#" class="twitter"><i class="fa fa-twitter"></i></a>
-->
                <a href="#" class="facebook" target="_blank"><i class="fa fa-facebook"></i></a>
                <a href="https://instagram.com/lavoreservicos" class="instagram" target="_blank"><i class="fa fa-instagram"></i></a>
<!--                
                <a href="#" class="google-plus"><i class="fa fa-google-plus"></i></a>
                <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
-->
              </div>
            </div>
          </div>

          <div class="col-lg-3 col-md-4">
            <div class="info">
              <div>
                <i class="ion-ios-location-outline"></i>
                <p>Rua tal<br>São Paulo, SP 02326-100</p>
              </div>

              <div>
                <i class="ion-ios-email-outline"></i>
                <p>contao@lavoremarmores.com.br</p>
              </div>

              <div>
                <i class="ion-ios-telephone-outline"></i>
                <p>(11) 94726-8080</p>
              </div>

            </div>
          </div>

          <div class="col-lg-5 col-md-8">
            <div class="form">
              <div id="sendmessage">Sua mensagem foi enviada. Obrigado!</div>
              <div id="errormessage"></div>
              <form action="envia_mensagem.php" method="post" role="form" class="contactForm">
                <div class="form-row">
                  <div class="form-group col-lg-6">
                    <input type="text" name="nome" class="form-control" id="name" placeholder="Nome" data-rule="minlen:4" data-msg="Por favor inserir nome!" required="required"/>
                    <div class="validation"></div>
                  </div>
                  <div class="form-group col-lg-6">
                    <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" data-rule="email" data-msg="Por favor preencher com um e-mail valido!" required="required"/>
                    <div class="validation"></div>
                  </div>
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="assunto" id="subject" placeholder="Assunto" data-rule="minlen:4" data-msg="Por favor inserir um assunto com no minimo 8 caracteres!" required="required"
                  <div class="validation"></div>
                </div>
                <div class="form-group">
                  <textarea class="form-control" name="mensagem" rows="5" data-rule="required" data-msg="Por favor digite sua mensagem!" placeholder="Mensagem" required="required"></textarea>
                  <div class="validation"></div>
                </div>
                <div class="text-center"><button type="submit" title="Enviar Mensagem" name="BTEnvia" class="btn-enviar">Enviar Mensagem</button></div>
              </form>
            </div>
          </div>

        </div>

      </div>
    </section><!-- #contact -->

  </main>

  <!--==========================
    Footer
  ============================-->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-lg-6 text-lg-left text-center">
          <div class="copyright">
            &copy; Copyright <strong>LAVORE</strong>. Todos seus direitos reservados!
          </div>
          <div class="credits">
             Criado por <a href="http://www.micheldeziderio.com.br/" target="_blanck">Michel Deziderio</a>
          </div>
        </div>
        
      </div>
    </div>
  </footer><!-- #footer -->

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/easing/easing.min.js"></script>
  <script src="lib/wow/wow.min.js"></script>
  <script src="lib/superfish/hoverIntent.js"></script>
  <script src="lib/superfish/superfish.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>

  <!-- Contact Form JavaScript File -->
  

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>
</html>
